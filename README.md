# image

DevOps helper images

## Build locally
```bash
export service=lint ;docker build -t $service:debug1 $service ;docker run --rm -it --name $service docker.io/library/lint:debug1 bash
```

```fish
set service angular ;docker build -t $service:debug1 $service ;docker run --rm -it --name $service docker.io/library/$service:debug1 bash
set service common-build-tools ;docker build -t $service:debug1 $service ;docker run --rm -it --name $service docker.io/library/$service:debug1 bash
set service lint ;docker build -t $service:debug1 $service ;docker run --rm -it --name $service docker.io/library/$service:debug1 bash

```
